install.packages("ExtDist")
library(ExtDist)



p<-0.4
choose(50, 4)*p^4*(1-p)^46
1e-3

####Binomial distribution

#x is the success count
#N is the sample size
calc_binom <- function(x,N){
  p<-seq(0,1,by=1e-4)
  y<-choose(N,x)*(p^x)*(1-p)^(N-x)
  plot(p,y)
}

calc_binom_log <- function(x,N){
  p<-seq(0,1,by=1e-4)
  y<-log(choose(N,x)*(p^x)*(1-p)^(N-x))
  plot(p,y)
}


calc_binom(2,25)
calc_binom_log(2,25)
calc_binom(4,50)
calc_binom(8,100)
calc_binom_log(8,100)
calc_binom_log(2,25)

######Normal Distribution

calc_norm<- function(low,up,y_s,stdev){
  mu_head <- seq(low,up,by=1e-2)
  likelihood <- 1
  for (y in y_s){
    likelihood <- likelihood * dnorm(y,mean = mu_head, sd = stdev)
  }
  plot(mu_head,likelihood)
  likelihood_df<-data.frame(mu_head,likelihood)
  mle<-max(likelihood_df[2])
  print(subset(likelihood_df, likelihood_df[2] == mle))
}

mu <- 5
stdev <- 2
y_s <- rnorm(10,mean=mu,sd = stdev)
calc_norm(0,10,y_s,stdev)
print(mean(y_s))

####Poisson distrubtion

calc_pois <- function(min,max,yi_s){
  likelihood <- 1
  lambda_head <- seq(min,max,by=1e-2)
  for (y in yi_s){
    likelihood <- likelihood*dpois(y,lambda_head)
  }
  plot(lambda_head,likelihood)
  likelihood_df<-data.frame(lambda_head,likelihood)
  mle<-max(likelihood_df[2])
  print(subset(likelihood_df, likelihood_df[2] == mle))
}


lambda <- 5
yi_s<-rpois(300,lambda = lambda)
calc_pois(0,10,yi_s)
print(mean(yi_s))

###Laplace distribution
d_laplace <- dLaplace(-3,mu = 1,b = 5)
d_laplace2 <- dLaplace(5,mu = 1,b = 5)



calc_laplace<-function(min,max,yi_s,b){
  likelihood <- 1
  mu_head <- seq(min,max,by=1e-2)
  for (y in yi_s){
    likelihood <- likelihood*dLaplace(y,mu = mu_head,b)
  }
  plot(mu_head,likelihood)
  likelihood_df<-data.frame(mu_head,likelihood)
  mle<-max(likelihood_df[2])
  print(subset(likelihood_df, likelihood_df[2] == mle))
}
mu <- 5
b <- 2
y_i1<-rLaplace(250,mu = mu, b= b)
dist <- y_i1 - mu
y_i2 <- mu - dist

calc_laplace(mu -1, mu +1, y_i1 , b)
calc_laplace(mu -1, mu +1, y_i2 , b)

calc_laplace(mu -1, mu +1, 3 , b)
calc_laplace(mu -1, mu +1, 7 , b)

y_i1<-c(-2.5,0.25,0.75,1.25)
y_i2<-c(-1.25,0.25,0.75,2.5)
est.par <- eLaplace(y_i1, method="analytic.MLE")
est.par2 <- eLaplace(y_i2, method="analytic.MLE")
calc_laplace(-2, +2, y_i1 , b)
calc_laplace(-2, +2, y_i2 , b)


#set.seed(13)
n = 7
x = runif(n, -3, 3)
# x = c(-1.25,0.25,0.75)
f = function(mu) sum(abs(x - mu))
f_prime = function(mu) -sum(sign(x - mu))
mu = seq(-5, 5, 0.05)
median(x)
values_df<-data.frame(mu,sapply(mu, f))
min_val<-min(values_df[2])
print(subset(values_df, values_df[2] == min_val))
plot(mu, sapply(mu, f), type = "l", xlab="mu", ylab="K(mu)")
for (i in x) {
  abline(v=i, col="red")
}
plot(mu, sapply(mu, f_prime), xlab="mu", ylab="K'(mu)", cex=0.2, pch=20)
for (i in x) {
  abline(v=i, col="red")
}

values_df<-data.frame(mu,sapply(mu, f_prime))
min_val<-min(abs(values_df[2]))
print(subset(values_df, values_df[2] == min_val))



###Strong Likelihood

calc_binom2 <- function(x,N){
  p<-setdiff(seq(0,1,by=1e-4),c(0,1))
  # y<-log(choose(N,x)*(p^x)*(1-p)^(N-x))
  # y<-log(dbinom(x,N,p))
  likelihood<-dbinom(x =x,size =N,prob = p)
  likelihood_df<-data.frame(p,likelihood)
  mle<-max(likelihood_df[2])
  print(subset(likelihood_df, likelihood_df[2] == mle))
  likelihood = likelihood/mle
  plot(p,likelihood)
}

calc_nbinom<-function(x,N){
  p<-setdiff(seq(0,1,by=1e-4),c(0,1))
  # y<-log(choose(N,x)*(p^x)*(1-p)^(N-x))
  # y<-log(dnbinom(x =x,size =N,prob = 1-p))
  likelihood<-dnbinom(x =N-suc,size =suc,prob = p)
  likelihood_df<-data.frame(p,likelihood)
  mle<-max(likelihood_df[2])
  print(subset(likelihood_df, likelihood_df[2] == mle))
  likelihood = likelihood/mle
  plot(p,likelihood)
}

N<-50
p<-0.08
suc<-rbinom(1,N,p)
calc_binom2(suc,N)
calc_nbinom(suc,N)
dnbinom(4,50,0.0000)




#### Equivariance
calc_binom_neg <- function(x,N){
  p<-seq(0,-1,by=-1e-4)
  y<-choose(N,x)*(-p^x)*(1+p)^(N-x)
  plot(p,y)
}
calc_binom <- function(x,N){
  p<-seq(0,1,by=1e-4)
  y<-choose(N,x)*(p^x)*(1-p)^(N-x)
  plot(p,y)
}

calc_binom_neg(2,6)
calc_binom(2,6)

calc_binom_neg(3,10)
calc_binom(3,10)


#####Method of Moments
mom_unif<-function(sample){
  N<- length(sample)
  m1 <- sum(sample)/N
  m2 <- sum(sample^2)/N
 theta<-sqrt(3*m2 - 3*m1^2) + m1
 return(theta)
}


mle_unif <- function(sample){
  return(max(sample))
}

lklhood_unif<-function(sample,max){
  likelihood = 1
  N = length(sample)
  theta = seq(max-2, max+5, 0.01)
  for(i in seq(1:N)){
    likelihood = likelihood*dunif(sample[i],min=0,max=theta)
  }
  plot(theta,likelihood)
}


err1 = err2 = 0
for(i in seq(1:1000)){
theta<-5
N<-10000
sample<-runif(n = N, min = 0 , max = theta)
theta1<-mom_unif(sample)
theta2<-mle_unif(sample)
err1 = err1 + (theta1-theta)^2
err2 = err2 + (theta2-theta)^2
# print(theta1)
# print(theta2)
}


#####Computational aspects

theta<-5
N<-100
sample<-runif(n = N, min = 0 , max = theta)
theta1<-mom_unif(sample)



log_uniformdrv<-function(n,theta){
  return(-n/theta)
}
log_uniformdrv2<-function(n,theta){
  return(n/(theta^2))
}
newton_uniform<-function(theta0,err,iter,func, func_drv,n){
  thetas = c(theta0)
  c_err = err*2
  for(i in seq(1:iter)){
    if(c_err > err ){
    theta_hat = thetas[i] - func(n,thetas[i])/func_drv(n,thetas[i])
    thetas = c(thetas,theta_hat)
    c_err = thetas[i+1] - thetas[i]
    }
  }
  return(thetas[length(thetas)])
}
log_uniformdrv2(N,theta1)

newton_gamma<-function(theta0,err,iter,func, func_drv,sample){
  thetas = c(theta0)
  c_err = err*2
  for(i in seq(1:iter)){
    if(c_err > err ){
      theta_hat = thetas[i] - func(thetas[i],sample)/func_drv(thetas[i],sample)
      thetas = c(thetas,theta_hat)
      c_err = thetas[i+1] - thetas[i]
    }
  }
  print(thetas)
  return(thetas[length(thetas)])
}

newton_uniform(theta1,0.0005,50,log_uniformdrv,log_uniformdrv2,N)
lklhood_unif(sample,5.5)

sample2 <- c(0.07,0.27,0.15,0.36,1.25,0.26,0.14,1.51,0.25,0.24)


log_drv_likelihood_gamma<-function(w,sample){
  muhead = mean(sample)
  n = length(sample)
  res<- sum(log(sample)) + n*log(w/muhead) -n*digamma(w)
  return(res)
}
log_drv2_likelihood_gamma<-function(w,sample){
  n = length(sample)
  res<- n/w - n*trigamma(w)
  return(res)
}
newton_gamma(theta0 = 0.5, 0.000000001,50,log_drv_likelihood_gamma,log_drv2_likelihood_gamma,sample2)

######Statistical Test procedures


##LRT
calc_binom_disc <- function(N,y,theta_space){
  lkl<-choose(N,y)*(theta_space^y)*(1-theta_space)^(N-y)
  print(y)
  print(c(lkl,lkl[1]/lkl[2]))
  plot(theta_space,lkl)

}

p = seq(from=0.3,to=0.35, by = 0.001)
theta_space<-c(0.3,0.35)
y = c()
N <- 10
for(i in p){
  y_sample=rbinom(1,N,p = i)
  y <- c(y,y_sample)
  calc_binom_disc(N,y_sample,theta_space)
}







lnorm <- function(theta,y){
  # theta ... theta[1]=mu theta[2]=sigma^2
  n <- length(y)
  mu <- theta[1]
  s2 <- theta[2]
  log_likelihood=(-n/2)*log(s2)-1/(2*s2)*(sum(y^2)-2*mu*sum(y)+n*mu^2)
  return(log_likelihood)
}
lrnorm <-function(theta,y){
  likelihood_ratio<-lnorm(theta,y)-lnorm(c(mean(y),sum(mean(y^2)-mean(y)^2)),y)
  #print(likelihood_ratio)
  return(likelihood_ratio)
  }
lplot2d <- function(y,thetamin,thetamax,nxpoints=100,nypoints=100){
  #plot relative likelihood
  xp<-seq(thetamin[1],thetamax[1],length.out=nxpoints)
  yp<-seq(thetamin[2],thetamax[2],length.out=nypoints)
  z <- matrix(0,ncol=nxpoints,nrow=nypoints)
  for(i in 1:nxpoints)
    for(j in 1:nypoints){
      z[i,j]<-exp(lrnorm(c(xp[i],yp[j]),y))
    }
  contour(xp,yp,z)
  max = which(z == max(z), arr.ind = TRUE)  
  print(max)
  mu_head<-xp[max[1]]
  sigma_head<-yp[max[2]]
  print(mu_head)
  print(sigma_head)
  points(mu_head,sigma_head,pch="+")
}
y<-rnorm(15)
print(mean(y))
print(var(y))
lplot2d(y,c(-1,0.0001),c(1,2))

#0.9188484*14/15 => unbiased estimator change for variance

