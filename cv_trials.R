samplesa<-rnorm(10,50,5)
samplesb<-rnorm(10,100,5)

mean(samplesa)
mean(samplesb)
cva<-sqrt(var(samplesa))/mean(samplesa)*100
cvb<-sqrt(var(samplesb))/mean(samplesb)*100

cva
cvb
pnorm(cva/100) - pnorm(-cva/100)
pnorm(cvb/100) - pnorm(-cvb/100)


a_generated<-rnorm(10,1,cva/100)*50
mean(a_generated)
var(a_generated)
sqrt(var(a_generated))
plot(a_generated)

a_generated2 <- rnorm(10,1,cvb/100)*50
mean(a_generated2)
var(a_generated2)
sqrt(var(a_generated2))
plot(a_generated2)

b_generated<-rnorm(10,1,cvb/100)*100
mean(b_generated)
var(b_generated)
sqrt(var(b_generated))

samplesa_new<- ((samplesa - mean(samplesa))/sqrt(var(samplesa))) +1
cva_new<-  sqrt(var(samplesa_new))/mean(samplesa_new)*100

samplesa_new2 <-  samplesa - mean(samplesa)*((length(samplesa)-1)/length(samplesa))
mean(samplesa_new2)
samplesa_new2<-samplesa_new2/sqrt(var(samplesa))
sqrt(var(samplesa))/sum(samplesa_new2)*
  
  
  
####Can coefficient of variance be used to measure skewness  
house_price<-c(2000000,500000,300000,100000,100000)
mean(house_price)
sqrt(var(house_price))/mean(house_price)*100

